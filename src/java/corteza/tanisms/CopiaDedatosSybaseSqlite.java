/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corteza.tanisms;

import java.util.Date;
import java.util.List;
import javax.ejb.Schedule;
import javax.ejb.Stateless;

/**
 *
 * @author sergi
 */
@Stateless
public class CopiaDedatosSybaseSqlite {

    @Schedule( month = "*", hour = "*", dayOfMonth = "*", year = "*", minute = "*/5", second = "0", persistent = false)
    
    public void myTimer() {
        System.out.println("Timer event: " + new Date());
        
        Sybase dbsys = new  Sybase();
        Database dbsqlite = new Database();
        
        List<SMSCompleto> lsms = dbsys.selectAlldate();
        ListaSMSCompleto lllsms = new ListaSMSCompleto();
        lllsms.setLsms(lsms);
        dbsqlite.copiadosybasesqlite(lllsms);
       
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
