/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corteza.tanisms;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author sergio
 */
public class Database {
      static final Logger log = LogManager.getLogger(Logger.class.getName());
    
      private Connection connect() {
        // SQLite connection string
        
        String url = "jdbc:sqlite:/tanisms/database.db";
        Connection conn = null;
        try {
            Class.forName("org.sqlite.JDBC");
           
            conn = DriverManager.getConnection(url);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
          System.out.println(conn);
        return conn;
    }
    
        public void copiadosybasesqlite(ListaSMSCompleto lista){
          //aca borarr toÑdo los del dia
          String sql = "";
          List<SMSCompleto> lsmsc = lista.getLsms();
           try (Connection conn = this.connect()){
         sql= "delete from sms_envio where fecha_ingreso >   datetime(date('now')); ";
          PreparedStatement pstmt = conn.prepareStatement(sql); 
                pstmt.executeUpdate();
          //aca insertar toda la listaentoen
          String fechaRespuesta = "2000-01-01 00:00:00";
           String fechaProceso = "2000-01-01 00:00:00";
            String fechaIngreso ="2000-01-01 00:00:00";
            String mensaje = "";
          
            for (Iterator<SMSCompleto> iterator = lsmsc.iterator(); iterator.hasNext();) {
                SMSCompleto next = iterator.next();
                 if( next.getFecha_ingreso()!=null &&  next.getFecha_ingreso().length()>18)  fechaIngreso =  next.getFecha_ingreso().substring(0, 19);
                 if( next.getFecha_proceso()!=null &&  next.getFecha_proceso().length()>18)  fechaProceso =  next.getFecha_proceso().substring(0, 19);
                if( next.getFecha_respuesta()!=null &&  next.getFecha_respuesta().length()>18) fechaRespuesta  =  next.getFecha_respuesta().substring(0, 19);
               if(next.getMensaje()!=null) mensaje = next.getMensaje().replace("'", "''");
                sql= "insert into sms_envio (linea,mensaje ,fecha_ingreso ,estado ,fecha_proceso,respuesta,fecha_respuesta ) values ('"+next.getLinea()+"','"+mensaje+"','"+fechaIngreso+"',"+next.getEstado()+",'"+fechaProceso+"','"+next.getRespuesta()+"','"+fechaRespuesta+"')";
                this.log.info(sql);
                 pstmt = conn.prepareStatement(sql); 
                pstmt.executeUpdate();
            }
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            
        }
      }
                  
         
      
      
     public List<SMS> selectAll(){
     
         String sql = " select sms_envio_id , linea , mensaje  from sms_envio where estado = 0";
         List<SMS> lsms = new ArrayList<>();
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
           
            // loop through the result set
            while (rs.next()) {
                SMS s = new SMS();
                System.out.println(rs.getInt("sms_envio_id") +  "\t" + 
                                   rs.getString("linea") + "\t" +
                                   rs.getString("mensaje"));
                s.setId_sms(rs.getInt("sms_envio_id"));
                s.setLinea(rs.getString("linea"));
                s.setMensaje(rs.getString("mensaje"));
                lsms.add(s);
            }
            
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return lsms;
    }
     
     
      public void update() {
        String sql = "UPDATE sms_envio SET estado = 1 "
                
                + "WHERE estado = 0";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
 
         
            pstmt.executeUpdate();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
 
     public void insert(String linea, String mensaje) {
        //String sql = "insert into sms_envio (linea,mensaje,fecha_ingreso,estado) values ('595971667740','hola','2017-09-17 14:39:00',0)";
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:MM:SS");
         java.util.Date d = new java.util.Date();
         
         
        String sql = "insert into sms_envio (linea,mensaje,fecha_ingreso,estado) values (?,?,?,0)";
     
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
 
            pstmt.setString(1, linea);
            pstmt.setString(2, mensaje);
            pstmt.setString(3, sdf.format(d) );
            
            pstmt.executeUpdate();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
     
}


