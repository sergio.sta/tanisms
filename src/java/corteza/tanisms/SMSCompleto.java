/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corteza.tanisms;

/**
 *
 * @author sergio
 */
class SMSCompleto {
    private String linea;;
    private String mensaje;
    private Integer id_sms;
    private String fecha_ingreso;
    private Integer estado ;
    private String fecha_proceso;
    private String respuesta;
    private String fecha_respuesta;

    @Override
    public String toString() {
        return "SMSCompleto{" + "linea=" + linea + ", mensaje=" + mensaje + ", id_sms=" + id_sms + ", fecha_ingreso=" + fecha_ingreso + ", estado=" + estado + ", fecha_proceso=" + fecha_proceso + ", respuesta=" + respuesta + ", fecha_respuesta=" + fecha_respuesta + '}';
    }
   

    /**
     * @return the linea
     */
    public String getLinea() {
        return linea;
    }

    /**
     * @param linea the linea to set
     */
    public void setLinea(String linea) {
        this.linea = linea;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the id_sms
     */
    public Integer getId_sms() {
        return id_sms;
    }

    /**
     * @param id_sms the id_sms to set
     */
    public void setId_sms(Integer id_sms) {
        this.id_sms = id_sms;
    }

    /**
     * @return the fecha_ingreso
     */
    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    /**
     * @return the estado
     */
    public Integer getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    /**
     * @return the fecha_proceso
     */
    public String getFecha_proceso() {
        return fecha_proceso;
    }

    /**
     * @param fecha_proceso the fecha_proceso to set
     */
    public void setFecha_proceso(String fecha_proceso) {
        this.fecha_proceso = fecha_proceso;
    }

    /**
     * @return the respuesta
     */
    public String getRespuesta() {
        return respuesta;
    }

    /**
     * @param respuesta the respuesta to set
     */
    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    /**
     * @return the fecha_respuesta
     */
    public String getFecha_respuesta() {
        return fecha_respuesta;
    }

    /**
     * @param fecha_respuesta the fecha_respuesta to set
     */
    public void setFecha_respuesta(String fecha_respuesta) {
        this.fecha_respuesta = fecha_respuesta;
    }

    /**
     * @param fecha_ingreso the fecha_ingreso to set
     */
    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }
}
