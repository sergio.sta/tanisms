/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corteza.tanisms;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author sergio
 */
@Path("/insertar")
public class Insertar {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Insertar
     */
    public Insertar() {
    }

    /**
     * Retrieves representation of an instance of corteza.tanisms.Insertar
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of Insertar
     * @param content representation for the resource
     */
    @GET
    @Path("/test")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String test(@QueryParam("linea") String linea, @QueryParam("mensaje")String mensaje) {
         Sybase db = new  Sybase();
        //Database db = new Database();
        db.insert(linea, mensaje);
        return  "{\"resultado\": \"OK\"}";
    }
    
    @GET
    @Path("/respuesta")
    @Consumes(MediaType.APPLICATION_JSON)
    public String respuesta(@QueryParam("linea") String linea, @QueryParam("mensaje")String mensaje) {
         Sybase db = new  Sybase();
         Database sqlite = new Database();
         if(mensaje.contains("SMS")){
             
             this.test("0974626801", "EL TELEFONO SE QUEDO SIN SALDO..");
             sqlite.insertSINSALDO("0974626801", mensaje);
             
         }
         
         if(mensaje.contains("El numero destino")){
              
             //el destino no existe se debe extraer del mensaje
             linea = mensaje.replaceAll("\\D+","");
             db.respuesta_linea_inexistente(linea, mensaje);
             
         }
         
         
        //Database db = new Database();
        db.respuesta(linea, mensaje);
        return  "{\"resultado\": \"OK\"}";
    }
}
