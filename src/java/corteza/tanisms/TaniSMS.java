/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corteza.tanisms;

import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * REST Web Service
 *
 * @author sergio
 */
@Path("tanisms")
public class TaniSMS {
  static final Logger log = LogManager.getLogger(Logger.class.getName());
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of TaniSMS
     */
    public TaniSMS() {
    }

    /**
     * Retrieves representation of an instance of corteza.tanisms.TaniSMS
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
   

    public String getJson() {
        //Database db = new  Database();
         Sybase db = new  Sybase();
       List<SMS> s = db.selectAll();
       db.update();
        ListaSMS l = new ListaSMS();
        l.setLsms(s);
        Gson gson = new Gson();
        this.log.info(gson.toJson(l));
        return  gson.toJson(l);
    }

    /**
     * PUT method for updating or creating an instance of TaniSMS
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
