/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corteza.tanisms;

import java.util.List;

/**
 *
 * @author sergio
 */
class ListaSMSCompleto {
    private List<SMSCompleto> lsms;

    /**
     * @return the lsms
     */
    public List<SMSCompleto> getLsms() {
        return lsms;
    }

    /**
     * @param lsms the lsms to set
     */
    public void setLsms(List<SMSCompleto> lsms) {
        this.lsms = lsms;
    }
}
