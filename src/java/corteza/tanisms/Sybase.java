/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corteza.tanisms;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author sergio
 */
public class Sybase {
         static final Logger log = LogManager.getLogger(Logger.class.getName());
  
  /*     */   private Statement stmt;
  //public Connection conectarBD(HashMap params)
          public Connection conectarBD()  throws Exception
  {
    //Class.forName("com.sybase.jdbc3.jdbc.SybDriver");
    
     Class.forName("com.sybase.jdbc3.jdbc.SybDriver");
    Properties props = new Properties();
    //props.put("user", (String)params.get("User DB"));
    props.put("user", "sms");
    //props.put("password", (String)params.get("Pass DB"));
   
    props.put("password", ".sms.envio");
    //String url = "jdbc:sybase:Tds:" + params.get("IP DB") + ":" + params.get("Port DB");
  //  String url = "jdbc:sybase:Tds:" +"192.168.1.22" + ":" +"2638";
  String url = "jdbc:sybase:Tds:" +"192.168.7.250" + ":" +"2638";
   
    
    this.log.info(" url [" + url + "]");
    Connection conn = DriverManager.getConnection(url, props);
    conn.setAutoCommit(true);
    return conn;
  }
  
  public void desconectarBD(Connection pConn)
    throws SQLException
  {
    pConn.close();
  }
  
    public String test(String idLog_) throws Exception
/*     */   {
            Connection  conexion = null;
/* 147 */     ResultSet resultSet = null;
/* 148 */     String query = null;
/*     */     String respuesta = "test";
/*     */     try
/*     */     {
                 conexion = this.conectarBD();
/* 152 */       log.info("[" + idLog_ + "] Preparando la funcion [traer fecha hora]");
/*     */       
 
       ResultSet rs = conexion.createStatement().executeQuery("select getdate() ;");
       
       
       if (rs.next()) {
           respuesta = rs.getString(1);
       }   else {
         log.info("[" + idLog_ + "] resultado: [error]");
      }
       rs.close();
       conexion.close();
   }

/*     */     catch (Exception ex) {
/* 178 */       log.error("[" + idLog_ + "] ACTIVACION - Error #5[" + ex.getMessage() + "]");
                ex.printStackTrace();
/* 179 */       conexion.close();
/*     */     }
/* 182 */     log.info("[" + idLog_ + "] respuesta [" + respuesta + "]");
             
/* 183 */     return respuesta;
/*     */   }
    
    
      public List<SMSCompleto> selectAlldate(){
     
         String sql = " select  sms_envio_id , linea , mensaje , fecha_ingreso , fecha_proceso, estado , respuesta, fecha_respuesta FROM DBA.sms_envio where fecha_ingreso > dateadd(day,0, convert(date,getdate()) ); ";
         List<SMSCompleto> lsms = new ArrayList<>();
         
        try {
           Connection conn = this.conectarBD();
           this.log.info("ejecutar "+ sql);
           Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql);
            // loop through the result set
            this.log.info("ejecutado ");
            while (rs.next()) {
                SMSCompleto s = new SMSCompleto();
                System.out.println(rs.getInt("sms_envio_id") +  "\t" + 
                                   rs.getString("linea") + "\t" +
                                   rs.getString("mensaje"));
                s.setId_sms(rs.getInt("sms_envio_id"));
                s.setLinea(rs.getString("linea"));
                s.setMensaje(rs.getString("mensaje"));
                s.setFecha_ingreso(rs.getString("fecha_ingreso"));
                s.setFecha_proceso(rs.getString("fecha_proceso"));
                s.setFecha_respuesta(rs.getString("fecha_respuesta"));
                s.setEstado(rs.getInt("estado"));
                s.setRespuesta(rs.getString("respuesta"));
                lsms.add(s);
            }
            
            conn.close();
        } catch (Exception e) {
            this.log.error(e.getMessage());
            e.printStackTrace();
            
        }
        
        return lsms;
    }
      
      
      
       public List<SMS> selectAll(){
     
         String sql = " select top(10) sms_envio_id , linea , mensaje  FROM DBA.sms_envio where  estado = 0 order by sms_envio_id desc; ";
         List<SMS> lsms = new ArrayList<>();
         
        try {
           Connection conn = this.conectarBD();
           this.log.info("ejecutar "+ sql);
           Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql);
            // loop through the result set
            this.log.info("ejecutado ");
            while (rs.next()) {
                SMS s = new SMS();
                System.out.println(rs.getInt("sms_envio_id") +  "\t" + 
                                   rs.getString("linea") + "\t" +
                                   rs.getString("mensaje"));
                s.setId_sms(rs.getInt("sms_envio_id"));
                s.setLinea(rs.getString("linea"));
                s.setMensaje(rs.getString("mensaje"));
                lsms.add(s);
            }
            
            conn.close();
        } catch (Exception e) {
            this.log.error(e.getMessage());
            e.printStackTrace();
            
        }
        
        return lsms;
    }
     
    
      
     
      public void update() {
        String sql = "UPDATE top(10) \"DBA\".\"sms_envio\" SET estado = 1 , fecha_proceso = getdate() WHERE estado = 0 order by sms_envio_id desc; ";
        this.log.info("preparando actualizacion");
               
 
        try  {
 
            Connection conn = this.conectarBD();
            PreparedStatement pstmt = conn.prepareStatement(sql);
             this.log.info("preparando actualizacionvvv");
            pstmt.executeUpdate();
              this.log.info("fin... actualizacion");
            conn.close();
        } catch (Exception e) {
             this.log.error(e.getMessage());
            e.printStackTrace();
            
        }
    }
      
      
      
      
      
      
        public void insert(String linea, String mensaje) {
        //String sql = "insert into sms_envio (linea,mensaje,fecha_ingreso,estado) values ('595971667740','hola','2017-09-17 14:39:00',0)";
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:MM:SS");
         java.util.Date d = new java.util.Date();
         
         
        String sql = "insert into  \"DBA\".\"sms_envio\"  (linea,mensaje,fecha_ingreso,estado) values ('"+linea+"','"+mensaje+"',getdate(),0)";
     
       
        try  {
 
            Connection conn = this.conectarBD();
            PreparedStatement pstmt = conn.prepareStatement(sql);
             this.log.info("preparando insercion");
            pstmt.executeUpdate();
              this.log.info("fin... insercion");
            conn.close();
        } catch (Exception e) {
             this.log.error(e.getMessage());
            e.printStackTrace();
            
        }
        
        
    }
        
        
          public void respuesta_linea_inexistente(String linea, String mensaje) {
        //String sql = "insert into sms_envio (linea,mensaje,fecha_ingreso,estado) values ('595971667740','hola','2017-09-17 14:39:00',0)";
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:MM:SS");
         java.util.Date d = new java.util.Date();
         
         
        String sql = "insert into  \"DBA\".\"sms_envio_lineas_inactivas\"  (linea,mensaje,fecha_ingreso) values ('"+linea+"','"+mensaje+"',getdate())";
     
       
        try  {
 
            Connection conn = this.conectarBD();
            PreparedStatement pstmt = conn.prepareStatement(sql);
             this.log.info("preparando insercion");
            pstmt.executeUpdate();
              this.log.info("fin... insercion");
            conn.close();
        } catch (Exception e) {
             this.log.error(e.getMessage());
            e.printStackTrace();
            
        }
        
        
    }
        
         public void respuesta(String linea, String mensaje) {
        //String sql = "insert into sms_envio (linea,mensaje,fecha_ingreso,estado) values ('595971667740','hola','2017-09-17 14:39:00',0)";
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:MM:SS");
         java.util.Date d = new java.util.Date();
         
         if(linea.contains("974626801")){
             //el destino no existe se debe extraer del mensaje
             linea = mensaje.replaceAll("\\D+","");
             
         }
         //se asume que viene +595971172990
         if(linea.length()>12){
                 linea = linea.substring(4);
         }else if (linea.length()>11) {
             linea = linea.substring(3);
         }
         
        String sql = "UPDATE dba.sms_envio\n" +
"set respuesta = respuesta || '###' || '"+mensaje+"' , estado = 2 , fecha_respuesta = today()\n" +
"where sms_envio_id in (\n" +
"select top (1) sms_envio_id  from dba.sms_envio where linea like ('%"+linea+"') order by 1 desc) ";
     
       
        try  {
 
            Connection conn = this.conectarBD();
            PreparedStatement pstmt = conn.prepareStatement(sql);
             this.log.info("preparando insercion");
            pstmt.executeUpdate();
              this.log.info("fin... insercion");
            conn.close();
        } catch (Exception e) {
             this.log.error(e.getMessage());
            e.printStackTrace();
            
        }
        
        
    }
}
