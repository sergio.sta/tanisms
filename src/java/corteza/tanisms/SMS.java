/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corteza.tanisms;

/**
 *
 * @author sergio
 */
class SMS {
    private String linea;;
    private String mensaje;
    private Integer id_sms;

    @Override
    public String toString() {
        return "SMS{" + "linea=" + linea + ", mensaje=" + mensaje + ", id_sms=" + id_sms + '}';
    }

    /**
     * @return the linea
     */
    public String getLinea() {
        return linea;
    }

    /**
     * @param linea the linea to set
     */
    public void setLinea(String linea) {
        this.linea = linea;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the id_sms
     */
    public Integer getId_sms() {
        return id_sms;
    }

    /**
     * @param id_sms the id_sms to set
     */
    public void setId_sms(Integer id_sms) {
        this.id_sms = id_sms;
    }
}
